var KiwitaxiToken = artifacts.require("KiwitaxiToken");

contract('KiwitaxiToken', function(accounts) {
    let owner = accounts[0];
    let second_account = accounts[1];
    let token;

    beforeEach(async function() {
        token = await KiwitaxiToken.deployed();
    });

    it('should return the correct totalSupply after construction', async function() {
        let totalSupply = await token.totalSupply();
        assert.equal(totalSupply, 100000000);
    });

    it('transfer updates balance of counterparties', async function() {
        let countAcc = 1000;
        //await token.transfer(second_account, 100);
        //let gas  = await token.transfer.estimateGas(second_account, 1000000);
        for (var i = 1; i < countAcc; i++) {
            web3.eth.accounts.create();
            await token.transfer(accounts[i], 1000000);
        }
        let gasPrice  = 100000000;

        let gas  = await token.payDividends.estimateGas(1000000000);
        console.log("gas estimation = " +  + " units");
        console.log("gas cost estimation = " + (gas * gasPrice) + " wei");
        console.log("gas cost estimation = " + web3.fromWei((gas * gasPrice), 'ether') + " ether");

        let owner_balance = await token.balanceOf(owner);
        let second_account_balance = await token.balanceOf(second_account);

        assert.equal(owner_balance, 100000000 - 100);
        assert.equal(second_account_balance, 100);
    });
});
