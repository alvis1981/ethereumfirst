pragma solidity ^ 0.4.18;


import "./StandartToken.sol";
import "./Owner.sol";
import "../library/IterableMapping.sol";
import "../library/SafeMath.sol";

contract KiwitaxiToken is StandardToken, Ownable {

    using SafeMath for uint256;

    string public constant name = "Kiwitaxi Coin Token";

    string public constant symbol = "KWT";

    uint32 public constant decimals = 18;

    uint256 public INITIAL_SUPPLY = 100000000000000000;

    function KiwitaxiToken() public {
        totalSupply = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
    }

    // Computes the sum of all stored data.
    function payDividends(uint256 payTokens) public onlyOwner returns (bool)
    {
        require((balanceOf(msg.sender) > payTokens));
        for (uint  i = 0; i < IterableMapping.size(data); i++){
            var addr = IterableMapping.getKeyByIndex(data, i);
            uint256 currentBalance = balanceOf(addr);
            if (currentBalance > 0 && msg.sender != addr) {
                uint256 amount = payTokens.mul(currentBalance.div(totalSupply));
                transfer(addr, amount);
            }
        }
        return true;
    }


}
